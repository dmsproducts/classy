Classy
==================

A utility and design class plugin for PageLines DMS. To use, simply add the class of your choice to the Standard Options -- Styling Classes field.
http://screencast.com/t/BFEzXqnW

A full list can be found in DMS under Global Options-->Classy.
